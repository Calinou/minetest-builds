#!/bin/bash
#
# This build script is licensed under CC0 1.0 Universal:
# https://creativecommons.org/publicdomain/zero/1.0/

set -euo pipefail

export DIR
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$DIR/../_common.sh"

# Build Linux server

cmake . \
      -DCMAKE_BUILD_TYPE="Release" \
      -DCMAKE_INSTALL_PREFIX="$(pwd)/appdir/usr" \
      -DCMAKE_C_COMPILER="clang" \
      -DCMAKE_CXX_COMPILER="clang++" \
      -DBUILD_CLIENT=0 \
      -DBUILD_SERVER=1 \
      -DENABLE_SYSTEM_JSONCPP=1 \
      -G "Ninja"
cmake --build . -- install

# Create Linux server AppImage

mv "appdir/usr/bin/minetestserver" "appdir/usr/bin/minetest-server"
strip "appdir/usr/bin/minetest-server"
mv "$CI_PROJECT_DIR/resources/minetest-server.desktop" "appdir/minetest-server.desktop"
mv "appdir/usr/share/icons/hicolor/scalable/apps/minetest.svg" "appdir/usr/share/icons/minetest-server.svg"
# Remove extraneous files
rm -rf \
    "appdir/usr/share/applications/" \
    "appdir/usr/share/doc/" \
    "appdir/usr/share/icons/hicolor/" \
    "appdir/usr/share/man" \
    "appdir/usr/share/metainfo"
wget -q "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage"
chmod +x "linuxdeployqt-continuous-x86_64.AppImage"
./linuxdeployqt-continuous-x86_64.AppImage --appimage-extract
./squashfs-root/AppRun "appdir/minetest-server.desktop" -appimage

mv "Minetest_Server-x86_64.AppImage" "$ARTIFACTS_DIR/minetest-server-0.5.0-dev-x86_64.AppImage"
