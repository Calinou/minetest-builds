#!/bin/bash
#
# This build script is licensed under CC0 1.0 Universal:
# https://creativecommons.org/publicdomain/zero/1.0/

set -euo pipefail

export DIR
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$DIR/../_common.sh"

# Build Linux client

cmake . \
      -DCMAKE_BUILD_TYPE="Release" \
      -DCMAKE_INSTALL_PREFIX="$(pwd)/appdir/usr" \
      -DCMAKE_C_COMPILER="clang" \
      -DCMAKE_CXX_COMPILER="clang++" \
      -DBUILD_CLIENT=1 \
      -DBUILD_SERVER=0 \
      -DENABLE_SYSTEM_JSONCPP=1 \
      -G "Ninja"
cmake --build . -- install

# Create Linux client AppImage

strip "appdir/usr/bin/minetest"
mv "appdir/usr/share/applications/net.minetest.minetest.desktop" "appdir/minetest.desktop"
mv "appdir/usr/share/icons/hicolor/scalable/apps/minetest.svg" "appdir/usr/share/icons/minetest.svg"
# Remove extraneous files
rm -rf \
    "appdir/usr/share/applications/" \
    "appdir/usr/share/doc/" \
    "appdir/usr/share/icons/hicolor/" \
    "appdir/usr/share/man" \
    "appdir/usr/share/metainfo"
wget -q "https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage"
chmod +x "linuxdeployqt-continuous-x86_64.AppImage"
./linuxdeployqt-continuous-x86_64.AppImage --appimage-extract
./squashfs-root/AppRun "appdir/minetest.desktop" -appimage

mv "Minetest-x86_64.AppImage" "$ARTIFACTS_DIR/minetest-0.5.0-dev-x86_64.AppImage"
