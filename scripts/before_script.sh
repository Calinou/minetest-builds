#!/bin/bash
#
# This build script is licensed under CC0 1.0 Universal:
# https://creativecommons.org/publicdomain/zero/1.0/

set -euo pipefail

export DIR
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$DIR/_common.sh"

# Install dependencies and prepare stuff before building

apt-get update -yqq
apt-get install -y build-essential libirrlicht-dev cmake libbz2-dev libpng-dev \
                   libjpeg-dev libxxf86vm-dev libgl1-mesa-dev libsqlite3-dev \
                   libogg-dev libvorbis-dev libopenal-dev libcurl4-gnutls-dev \
                   libfreetype6-dev zlib1g-dev libgmp-dev libjsoncpp-dev \
                   libluajit-5.1-dev libspatialindex-dev libleveldb-dev \
                   libpq-dev libhiredis-dev openssh-client git ninja-build \
                   clang

git clone --depth=1 "https://github.com/minetest/minetest.git"
git clone --depth=1 "https://github.com/minetest/minetest_game.git"
mkdir -p "$ARTIFACTS_DIR"
